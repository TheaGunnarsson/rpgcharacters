﻿using RPGcharacters.Attributes;
using RPGcharacters.Heros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RPGcharactersTests
{
    public class CharacterAttributeAndLevelTests
    {
        [Fact]
        public void InitOfCharacter_SetsCorrectLevel()
        {
            //A character is level 1 when created.
            //Arrange
            Mage mage = new("Best Mage in the World");
            int expected = 1;
            //Act
            int actual = mage.Level;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void InitOfMage_SetsCorrectAttributes()
        {
            //Each character class is created with the proper default attributes
            //Arrange
            Mage mage = new("Best Mage in the World");
            PrimaryAttributes expected = new() { Strenght = 1, Dexterity = 1, Intelligence = 8 };
            //Act
            PrimaryAttributes actual = mage.BasePrimaryAttributes;
            //Assert
            Assert.True(expected.Strenght == actual.Strenght && expected.Dexterity == actual.Dexterity && expected.Intelligence == actual.Intelligence);
        }
        [Fact]
        public void InitOfWarrior_SetsCorrectAttributes()
        {
            //Each character class is created with the proper default attributes
            //Arrange
            Warrior warrior = new("Best Warrior in the World");
            PrimaryAttributes expected = new() { Strenght = 5, Dexterity = 2, Intelligence = 1 };
            //Act
            PrimaryAttributes actual = warrior.BasePrimaryAttributes;
            //Assert
            Assert.True(expected.Strenght == actual.Strenght && expected.Dexterity == actual.Dexterity && expected.Intelligence == actual.Intelligence);
        }
        [Fact]
        public void InitOfRanger_SetsCorrectAttributes()
        {
            //Each character class is created with the proper default attributes
            //Arrange
            Ranger ranger = new("Best Ranger in the World");
            PrimaryAttributes expected = new() { Strenght = 1, Dexterity = 7, Intelligence = 1 };
            //Act
            PrimaryAttributes actual = ranger.BasePrimaryAttributes;
            //Assert
            Assert.True(expected.Strenght == actual.Strenght && expected.Dexterity == actual.Dexterity && expected.Intelligence == actual.Intelligence);
        }
        [Fact]
        public void InitOfRouge_SetsCorrectAttributes()
        {
            //Each character class is created with the proper default attributes
            //Arrange
            Rouge rouge = new("Best Rouge in the World");
            PrimaryAttributes expected = new() { Strenght = 2, Dexterity = 6, Intelligence = 1 };
            //Act
            PrimaryAttributes actual = rouge.BasePrimaryAttributes;
            //Assert
            Assert.True(expected.Strenght == actual.Strenght && expected.Dexterity == actual.Dexterity && expected.Intelligence == actual.Intelligence);
        }
        //When a character gains a level, it should be level 2
        [Fact]
        public void LevelUp_CharacterLevelUp_SetsCorrectLevel()
        {
            // Arrange
            Mage mage = new("Cool mage");
            int expected = 2;
            // Act
            mage.LevelUp();
            int actual = mage.Level;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_LevelUpMage_SetsCorrectPrimaryAttributes()
        {
            // Arrange
            Mage mage = new("Mage");
            PrimaryAttributes expected = new() { Strenght = 2, Dexterity = 2, Intelligence = 13 };
            // Act
            mage.LevelUp();
            PrimaryAttributes actual = mage.BasePrimaryAttributes;
            //Assert
            Assert.True(expected.Strenght == actual.Strenght && expected.Dexterity == actual.Dexterity && expected.Intelligence == actual.Intelligence);
        }
        [Fact]
        public void LevelUp_LevelUpWarrior_SetsCorrectPrimaryAttributes()
        {
            // Arrange
            Warrior warrior = new("Warrior");
            PrimaryAttributes expected = new() { Strenght = 8, Dexterity = 4, Intelligence = 2 };
            // Act
            warrior.LevelUp();
            PrimaryAttributes actual = warrior.BasePrimaryAttributes;
            //Assert
            Assert.True(expected.Strenght == actual.Strenght && expected.Dexterity == actual.Dexterity && expected.Intelligence == actual.Intelligence);
        }
        [Fact]
        public void LevelUp_LevelUpRanger_SetsCorrectPrimaryAttributes()
        {
            // Arrange
            Ranger ranger = new("Ranger");
            PrimaryAttributes expected = new() { Strenght = 2, Dexterity = 12, Intelligence = 2 };
            // Act
            ranger.LevelUp();
            PrimaryAttributes actual = ranger.BasePrimaryAttributes;
            //Assert
            Assert.True(expected.Strenght == actual.Strenght && expected.Dexterity == actual.Dexterity && expected.Intelligence == actual.Intelligence);
        }
        [Fact]
        public void LevelUp_LevelUpRouge_SetsCorrectPrimaryAttributes()
        {
            // Arrange
            Rouge rouge = new("Rouge");
            PrimaryAttributes expected = new() { Strenght = 3, Dexterity = 10, Intelligence = 2 };
            // Act
            rouge.LevelUp();
            PrimaryAttributes actual = rouge.BasePrimaryAttributes;
            //Assert
            Assert.True(expected.Strenght == actual.Strenght && expected.Dexterity == actual.Dexterity && expected.Intelligence == actual.Intelligence);
        }
    }
}
