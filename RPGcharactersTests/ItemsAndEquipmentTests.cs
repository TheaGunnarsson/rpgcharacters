﻿using RPGcharacters.Attributes;
using RPGcharacters.Heros;
using RPGcharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RPGcharactersTests
{
    public class ItemsAndEquipmentTests
    {
        Weapon testAxe = new Weapon()
        {
            ItemName = "Common axe",
            ItemLevel = 1,
            ItemSlot = Slot.slot_Weapon,
            WeaponType = WeaponType.weaponAxes,
            WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
        };
        Weapon testAxeLevel = new Weapon()
        {
            ItemName = "Common axe",
            ItemLevel = 2,
            ItemSlot = Slot.slot_Weapon,
            WeaponType = WeaponType.weaponAxes,
            WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
        };
        Armor testPlateBody = new Armor()
        {
            ItemName = "Common plate body armor",
            ItemLevel = 1,
            ItemSlot = Slot.slot_Body,
            Armortype = ArmorType.armorPlate,
            Attributes = new PrimaryAttributes() { Strenght = 1 }
        };
        Armor testPlateBodyLevel = new Armor()
        {
            ItemName = "Common plate body armor",
            ItemLevel = 2,
            ItemSlot = Slot.slot_Body,
            Armortype = ArmorType.armorPlate,
            Attributes = new PrimaryAttributes() { Strenght = 1 }
        };
        Weapon testBow = new Weapon()
        {
            ItemName = "Common bow",
            ItemLevel = 1,
            ItemSlot = Slot.slot_Weapon,
            WeaponType = WeaponType.weaponBows,
            WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
        };
        Armor testClothHead = new Armor()
        {
            ItemName = "Common cloth head armor",
            ItemLevel = 1,
            ItemSlot = Slot.slot_Head,
            Armortype = ArmorType.armorCloth,
            Attributes = new PrimaryAttributes() { Intelligence = 5 }
        };
        [Fact]
        public void Equip_CharacterTriesToEquipHighLevelWeapon_ThrowsInvalidWeaponException()
        {
            //Arrange
            Warrior warrior = new("Warrior");
            //act
            Action act = () => warrior.EquipItem(testAxeLevel);
            //assert
            InvalidWeaponExeption exception = Assert.Throws<InvalidWeaponExeption>(act);
        }
        [Fact]
        public void Equip_CharacterTriesToEquipHighLevelArmor_ThrowsInvalidArmorException()
        {
            //Arrange
            Warrior warrior = new("Warrior");
            //act
            Action act = () => warrior.EquipItem(testPlateBodyLevel);
            //assert
            InvalidArmorExeption exception = Assert.Throws<InvalidArmorExeption>(act);
        }
        [Fact]
        public void Equip_CharacterTriesToEquipWrongWeaponType_ThrowsInvalidWeaponException()
        {
            //Arrange
            Warrior warrior = new("Warrior");
            //act
            Action act = () => warrior.EquipItem(testBow);
            //assert
            InvalidWeaponExeption exception = Assert.Throws<InvalidWeaponExeption>(act);
        }
        [Fact]
        public void Equip_CharacterTriesToEquipWrongArmorType_ThrowsInvalidArmorException()
        {
            //Arrange
            Warrior warrior = new("Warrior");
            //act
            Action act = () => warrior.EquipItem(testClothHead);
            //assert
            InvalidArmorExeption exception = Assert.Throws<InvalidArmorExeption>(act);
        }
        [Fact]
        public void CharacterDPS_CalculateDPSToWarriorOfLevelOne_ReturnsCharactersDPS()
        {
            //Arrange
            Warrior warrior = new("Warrior");
            double expected = (double)1 * (1 + ((double)5 / 100)); 
            // Act
            double actual = warrior.CharacterDPS();
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CharacterDPS_CalculateDPSOfWarriorToLevelOneAndEquippedWeapon_ReturnsCharactersDPS()
        {
            //Arrange
            Warrior warrior = new("Warrior");
            double expected = (7 * 1.1) * (1 + ((double)5 / 100.0));
            // Act
            warrior.EquipItem(testAxe);
            double actual = warrior.CharacterDPS();
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalculateDPS_CalculateDPSOfhWarriorOfLevelOneAndEquippedWeaponAndArmor_ReturnsCharactersDPS()
        {
            //Arrange
            Warrior warrior = new("Warrior");
            double expected = (7 * 1.1) * (1 + ((double)(5 + 1) / 100.0));
            // Act
            warrior.EquipItem(testAxe);
            warrior.EquipItem(testPlateBody);
            double actual = warrior.CharacterDPS();
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Equip_CharacterTriesToEquipValidWeaponType_ReturnsSuccessMessage()
        {
            //Arrange
            Warrior warrior = new("Warrior");
            string expected = "New weapon equipped!";
            // Act
            string actual = warrior.EquipItem(testAxe);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equip_CharacterTriesToEquipValidArmorType_ReturnsSuccessMessage()
        {
            //Arrange
            Warrior warrior = new("Warrior");
            string expected = "New armor equipped!";
            // Act
            string actual = warrior.EquipItem(testPlateBody);
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
