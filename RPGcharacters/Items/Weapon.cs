﻿using RPGcharacters.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharacters.Items
{
    /// <summary>
    /// lists all weapons
    /// </summary>
    public enum WeaponType
    {
        weaponAxes,
        weaponBows,     //ranger
        weaponDaggers, //rounge
        weaponHammers, //warrior
        weaponStaffs,  //mage
        weaponSwords,   //rouge, warrior
        weaponWands     //mage
    }
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }

    }

}
