﻿using RPGcharacters.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharacters.Items
{
    //same as weapon to match with type
    //enum
    /// <summary>
    /// list all types of armor
    /// </summary>
    public enum ArmorType
    {
        armorCloth,  //mage
        armorLeather, //ranger, rouge
        armorMail,     //ranger, rouge, warrior 
        armorPlate      //warrior
    }
    public class Armor : Item
    {
        //enum as property
        public ArmorType Armortype { get; set; }
        public PrimaryAttributes Attributes { get; set; }

    }
}
