﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharacters.Items
{
    //Encapsulates a memory slot to store local data
    //Slot as an enumerator
    /// <summary>
    /// list of all places items can be equipped
    /// </summary>
    public enum Slot
    {
        slot_Head,
        slot_Body,
        slot_Legs,
        slot_Weapon
    }
    public abstract class Item
    {
        //properties
        public string ItemName { get; set; }
        public int ItemLevel { get; set; } //Required level to equip the item.
        public Slot ItemSlot { get; set; } //Slot in which the item is equipped

    }
}
