﻿using RPGcharacters.Heros;
using RPGcharacters.Items;
using System;

namespace RPGcharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            //Mage test = new Mage("nolla");
            //Mage mage = new Mage("hulda");
            Warrior warrior = new Warrior("Helge");
            //Console.WriteLine(mage.DisplayStats());
            //mage.LevelUp();
            //Console.WriteLine(mage.DisplayStats());
            //mage.LevelUp();
            //mage.EquipItem(CreateWeapon());
            //mage.WeaponDamagePerSecond();
            //test.WeaponDamagePerSecond();
            //mage.EquipItem(CreateArmor());
            //Console.WriteLine(mage.DisplayStats());
            //Console.WriteLine((7 * 1.1) * (1 + ((double)(5 + 1) / 100.0)));
            Console.WriteLine(warrior.DisplayStats());
            warrior.EquipItem(CreateArmor());
            //warrior.LevelUp();
            //warrior.LevelUp();
            Console.WriteLine(warrior.DisplayStats());
            //EquipItem(CreateWeapon());
            //CreateWeapon();

        }

        private static Weapon CreateWeapon()
        {
            Weapon test = new Weapon()
            {
                ItemName = "killer wand",
                ItemLevel = 1,
                ItemSlot = Slot.slot_Weapon, //remember to check this some where
                WeaponType = WeaponType.weaponWands,
                WeaponAttributes = new Attributes.WeaponAttributes() { Damage = 2, AttackSpeed = 1.1 }
            };
            return test;
        }
        private static Armor CreateArmor()
        {

            Armor test = new Armor()
            {
                ItemName = "shiny cloth",
                ItemLevel = 1,
                ItemSlot = Slot.slot_Body, 
                Armortype = ArmorType.armorPlate,
                Attributes = new Attributes.PrimaryAttributes() { Strenght=20, Dexterity=1, Intelligence=1}
            };
            return test;
        }
    }
}

