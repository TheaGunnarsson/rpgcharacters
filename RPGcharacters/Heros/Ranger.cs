﻿using RPGcharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharacters.Heros
{
    public class Ranger : Hero
    {
        //constructor initilize
        //using base string name, int strenght, int dexterity, int intelligence
        ///<summary>
        ///constructor initilize Ranger
        ///</summary>
        ///<param name="name">Heros name</param>
        ///<inheritdoc [cref="Heros/Hero"]/>
        public Ranger(string name):base(name, 1, 7, 1)
        {
               
        }
        /// <inheritdoc [cref="Heros/Hero"]/>
        public override void LevelUp()
        {
            BasePrimaryAttributes.Intelligence += 1;
            BasePrimaryAttributes.Dexterity += 5;
            BasePrimaryAttributes.Strenght += 1;
            Level += 1;
            CalculateTotalStats();
        }
        /// <inheritdoc [cref="Heros/Hero"]/>
        public override double CharacterDPS()
        {
            double CDPS = WeaponDamagePerSecond() * (1 + TotalPrimaryAttributes.Intelligence / 100);
            return CDPS;
        }
        /// <inheritdoc [cref="Heros/Hero"]/>
        public override string EquipItem(Armor armor)
        {
            if (armor.ItemLevel > Level)
            {
                throw new InvalidArmorExeption("character has too low level");
            }
            else if (armor.Armortype != ArmorType.armorMail && armor.Armortype != ArmorType.armorLeather) // || 
            {
                throw new InvalidArmorExeption("character cant have this armor");
            }
            else if(armor.ItemSlot != Slot.slot_Body && armor.ItemSlot != Slot.slot_Head && armor.ItemSlot != Slot.slot_Legs)
            {
                throw new InvalidArmorExeption("broken armor");
            }
            else
            {
                Equipment[armor.ItemSlot] = armor;
                CalculateTotalStats();
                return "New armor equipped!";
            }
        }
        /// <inheritdoc [cref="Heros/Hero"]/>
        public override string EquipItem(Weapon weapon)
        {
            if (weapon.ItemLevel > Level)
            {
                throw new InvalidWeaponExeption("Character has too low level");
            }
            else if (weapon.WeaponType != WeaponType.weaponBows)
            {
                throw new InvalidWeaponExeption("Character cant have this type of weapon");
            }
            //so you cant equip item in wrong slot
            else if(weapon.ItemSlot != Slot.slot_Weapon)
            {
                throw new InvalidWeaponExeption("broken weapon");
            }
            else
            {
                Equipment[weapon.ItemSlot] = weapon;
                return "New weapon equipped!";
            }
        }
    }
}
