﻿using RPGcharacters.Attributes;
using RPGcharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharacters.Heros
{
    public abstract class Hero
    {
        //properties for all heros
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; }//from class
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }//from class
        //the slot
        public Dictionary<Slot, Item> Equipment { get; set; }
        public double DPS { get; set; }

        /// <summary>
        /// constructor initializes a hero at level one.
        /// </summary>
        /// <param name="name">Name of hero</param>
        /// <param name="strenght">Strenght of hero</param>
        /// <param name="dexterity">Dexterity of hero</param>
        /// <param name="intelligence">Intelligence of hero</param>
        public Hero(string name, int strenght, int dexterity, int intelligence)
        {
            Name = name;
            Level = 1; //start at level one
            BasePrimaryAttributes = new PrimaryAttributes()
            {
                Strenght = strenght,
                Dexterity = dexterity,
                Intelligence = intelligence
            };
            Equipment = new Dictionary<Slot, Item>();
            CalculateTotalStats();
        }

        ///<summary>
        ///calculates total stats based on base attributes and equipped items
        ///</summary>
        public void CalculateTotalStats()
        {
            TotalPrimaryAttributes = ArmorBouns();
            DPS = CharacterDPS();
        }

        ///<summary>
        ///Equips weapon
        ///</summary>
        ///<param name="weapon">Weapon object</param>
        ///<exception cref="InvalidWeaponExeption">throws if weapon has too high level or if its wrong type for hero or if trying to equpit in wrong slot</exception>
        ///<returns>string with success message</returns>
        public abstract string EquipItem(Weapon weapon);

        ///<summary>
        ///equips armor
        ///</summary>
        ///<param name="armor">Armor object</param>
        ///<exception cref="InvalidArmorExeption">throws if armor has too high level or if its wrong type for hero or if trying to equipt in wrong slot</exception>
        ///<returns>string with success message</returns>
        public abstract string EquipItem(Armor armor);

        ///<summary>
        ///Levels up a hero and re-calculates its total stats
        ///</summary>
        public abstract void LevelUp();

        ///<summary>
        ///calculates heros damage per second 
        ///</summary>
        ///<returns>double with heros damage per second</returns>
        public abstract double CharacterDPS();

        ///<summary>
        ///calculates equipped weapons damage per second
        ///</summary>
        ///<returns>returns double with weapons damage per second</returns>
        public double WeaponDamagePerSecond()
        {
            Item weapon;
            if (Equipment.TryGetValue(Slot.slot_Weapon, out weapon))
            {
                Weapon weaponattr = (Weapon)weapon;
                return weaponattr.WeaponAttributes.DPS();
            }
            else
            {
                return 1;
            }
        }

        ///<summary>
        ///calculates equipped armors bonus  
        ///</summary>
        ///<returns>new primary attributes</returns>
        public PrimaryAttributes ArmorBouns()
        {
            PrimaryAttributes armorBonus = new() { Strenght = 0, Dexterity = 0, Intelligence = 0 };
            //count in the armor attributes to base attributes to get total attributes
            Item armor;
            if(Equipment.TryGetValue(Slot.slot_Head, out armor))
            {
                Armor armorHead = (Armor)armor;
                armorBonus.Intelligence = BasePrimaryAttributes.Intelligence += armorHead.Attributes.Intelligence;
                armorBonus.Strenght = BasePrimaryAttributes.Strenght += armorHead.Attributes.Strenght;
                armorBonus.Dexterity = BasePrimaryAttributes.Dexterity += armorHead.Attributes.Dexterity;
            }
            if (Equipment.TryGetValue(Slot.slot_Body, out armor))
            {
                Armor armorBody = (Armor)armor;
                armorBonus.Intelligence = BasePrimaryAttributes.Intelligence + armorBody.Attributes.Intelligence;
                armorBonus.Strenght = BasePrimaryAttributes.Strenght + armorBody.Attributes.Strenght;
                armorBonus.Dexterity = BasePrimaryAttributes.Dexterity + armorBody.Attributes.Dexterity;
            }
            if (Equipment.TryGetValue(Slot.slot_Legs, out armor))
            {
                Armor armorLegs = (Armor)armor;
                armorBonus.Intelligence = BasePrimaryAttributes.Intelligence += armorLegs.Attributes.Intelligence;
                armorBonus.Strenght = BasePrimaryAttributes.Strenght += armorLegs.Attributes.Strenght;
                armorBonus.Dexterity = BasePrimaryAttributes.Dexterity += armorLegs.Attributes.Dexterity;
            }
            else if(Equipment.Count == 0)
            {
                armorBonus.Intelligence = BasePrimaryAttributes.Intelligence;
                armorBonus.Strenght = BasePrimaryAttributes.Strenght;
                armorBonus.Dexterity = BasePrimaryAttributes.Dexterity;
            }
            return armorBonus;
        }

        ///<summary>
        ///adding values to string builder
        ///</summary>
        ///<returns>string with all values</returns>
        ///
        public string DisplayStats()
        {
            StringBuilder heroStats = new StringBuilder("****Heros stats****\n");
            // Append a format string to the end of the StringBuilder.
            heroStats.AppendFormat($"Name: {Name}\n");
            heroStats.AppendFormat($"Level: {Level}\n");
            heroStats.AppendFormat($"TOTAL Strenght: {TotalPrimaryAttributes.Strenght}\n");
            heroStats.AppendFormat($"TOTAL Intelligence: {TotalPrimaryAttributes.Intelligence}\n");
            heroStats.AppendFormat($"TOTAL Dexterity: {TotalPrimaryAttributes.Dexterity}\n");
            heroStats.AppendFormat($"DPS: {DPS}\n");
            heroStats.AppendFormat($"*******************\n");
            //Console.WriteLine(heroStats.ToString());
            return heroStats.ToString();
        }
    }
}
//a character can "try to equip any item in the game", there should be a method that can 
//verify if the certain character can equip that item.If not, 
//then throw invalidItemexception

//from slack channel
//Making a method abstract forces the subclasses to implement and override it.
//If you do a non-abstract method with an empty body you can override it but
//you are no longer forced to so you could potentially forget to override it and
//get logical errors that are hard to troubleshoot
