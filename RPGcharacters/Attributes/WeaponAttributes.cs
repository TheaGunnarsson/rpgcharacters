﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharacters.Attributes
{
    public class WeaponAttributes
    {
        //properties
        public double Damage { get; set; }
        public double AttackSpeed { get; set; }
        /// <summary>
        /// calculates weapon damage per second
        /// </summary>
        /// <returns>double with DPS</returns>
        public double DPS()
        {
            return Damage * AttackSpeed;
        }
    }
}
