﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharacters.Attributes
{
    public class PrimaryAttributes
    {
        //properties
        public double Strenght { get; set; }
        public double Dexterity { get; set; }
        public double Intelligence { get; set; }
    }
}
