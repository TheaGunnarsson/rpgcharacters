﻿using System;
using System.Runtime.Serialization;

namespace RPGcharacters.Heros
{
    [Serializable]
    //theese generated them selfes 
    public class InvalidWeaponExeption : Exception
    {
        public InvalidWeaponExeption()
        {
        }

        public InvalidWeaponExeption(string message) : base(message)
        {
        }

        public InvalidWeaponExeption(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidWeaponExeption(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}