﻿using System;
using System.Runtime.Serialization;

namespace RPGcharacters.Heros
{
    [Serializable]
    //theese generated them selfes 
    public class InvalidArmorExeption : Exception
    {
        public InvalidArmorExeption()
        {
        }

        public InvalidArmorExeption(string message) : base(message)
        {
        }

        public InvalidArmorExeption(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidArmorExeption(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}